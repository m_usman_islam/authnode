const User = require('../models/user');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

//import validations
const {
  userRegisterValidation,
  userLoginValidation,
} = require('../validations/user');




const createUser = async (req, res) => {

  const { first_name, last_name, email, age, password } = req.body;

  let error = userRegisterValidation(req.body);

  if (error) return res.status(200).json(error);

  const emailIsExist = await User.findOne({ email });
  if (emailIsExist) return res.status(200).json('Email already registerd');

  const salt = await bcrypt.genSalt(10);
  const hashedPassword = await bcrypt.hash(password, salt);

  try {
    const response = await User.create({
      first_name,
      last_name,
      email,
      age,
      password: hashedPassword,
    });

    res.json(response);
  } catch (e) {
    console.log('error', e);
  }
};


const loginUser = async (req, res) => {

  const { email, password } = req.body;
  let error = userLoginValidation(req.body);
  if (error) return res.status(200).json(error);

  const user = await User.findOne({ email });
  if (!user) return res.status(200).json('Email is not registed');

  const validPass = await bcrypt.compare(password, user.password);
  if (!validPass) return res.status(200).json('Invalid password');

  const token = await jwt.sign({ _id: user._id }, process.env.SECERET_TOKEN);

  res.json(token);
};


module.exports = { createUser, loginUser };
