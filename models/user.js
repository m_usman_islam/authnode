const mongoose = require('mongoose');

const resgistrationModel = mongoose.Schema(
  {

    first_name: {
      type: String,
      requeried: true,
      min: 6,
      max: 50,
    },

    last_name: {
      type: String,
      requeried: true,
      min: 6,
      max: 50,
    },

    email: {
      type: String,
      requeried: true,
    },
    age: {
      type: Number,
      requeried: true,
    },
    password: {
      type: String,
      min: 6,
      max: 1024,
      requeried: true,
    },
  },
  { timestamps: true }
  
);

module.exports = mongoose.model('User', resgistrationModel);
