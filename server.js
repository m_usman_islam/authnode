require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');

const app = express();
app.use(express.json());
//import routes

const userAuth = require('./routes/user');
const postsRoutes = require('./routes/posts');

app.use('/v1/api/user', userAuth);
app.use('/v1/api/posts', postsRoutes);

mongoose
  .connect(process.env.URI)
  .then(() => {
    app.listen(process.env.PORT, () => {
      console.log(`server has up ! on ${process.env.PORT} and db connected`);
    });
  })
  .catch((e) => {
    console.log(`Error are ${e}`);
  });
