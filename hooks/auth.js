const jwt = require('jsonwebtoken');

module.exports = async (req, res, next) => {
  const token = req.header('Authorization');

  if (!token) return res.json('unauthorized');

  try {
    const isVerfied = await jwt.verify(token, process.env.SECERET_TOKEN);

    if (isVerfied) req.userID = isVerfied;
    next();
  } catch (e) {
    res.json('Invalid Token');
  }
};
