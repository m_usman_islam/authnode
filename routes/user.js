const express = require('express');

const router = express.Router();

//import controller
const { createUser, loginUser } = require('../controllers/user');

//user Rigster api

router.post('/register', createUser);
router.post('/login', loginUser);
module.exports = router;
