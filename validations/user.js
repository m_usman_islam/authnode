const Joi = require('joi');

const userRegisterValidation = (data) => {
  const registerValidateSchema = Joi.object({
    first_name: Joi.string().min(6).max(50).required(),
    last_name: Joi.string().min(6).max(50).required(),
    age: Joi.number().min(1).max(100).required(),
    email: Joi.string().min(6).max(50).required().email(),
    password: Joi.string().min(6).max(1024).required(),
  });
  const { error } = registerValidateSchema.validate(data);
  return error;
};

const userLoginValidation = (data) => {
  const loginValidateSchema = Joi.object({
    email: Joi.string().min(6).max(50).required().email(),
    password: Joi.string().min(6).max(1024).required(),
  });
  const { error } = loginValidateSchema.validate(data);
  return error;
};

module.exports = { userRegisterValidation, userLoginValidation };
